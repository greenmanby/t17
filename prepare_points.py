import os
import sys
import json
import numpy as np
import scipy.spatial as sp

Day_struct = {"photos": [],
              "tracks": [],
              "stays": [],
              "bbox": {"NE_lat":90., "NE_lng":180., "SW_lat": 0., "SW_lng": 0.},
              }

class PT:
  def __init__(self):
    print("Generating the data...")

    self.firstday = np.datetime64('2017-%02i-%02i' % (8, 12))

    self.photo_points = []
    self.tracks = []
    self.stays = []
    self.calendar = {}
    self.tree = None

    CUR_DIR = os.path.dirname(os.path.abspath(__file__))

    with open(os.path.join(CUR_DIR,"photos.json"), "r") as f:
      self.photo_points = self.photo_points + json.loads(f.read())
    with open(os.path.join(CUR_DIR,"videos.json"), "r") as f:
      self.tracks = json.loads(f.read())
    with open(os.path.join(CUR_DIR,"overnight.json"), "r") as f:
      self.stays = json.loads(f.read())

    for p,i in zip(self.photo_points, range(len(self.photo_points))):
      p["name"] = p["name"].replace("/media/green/F0D449C8D4499230/video_sony/photo", "photo")
      p["thumb"] = p["name"].replace(os.path.splitext(p["name"])[1], "_thumb"+os.path.splitext(p["name"])[1])
      day = self.getDayOfTheTrip(p["jstime"][1], p["jstime"][2])
      if not self.calendar.has_key(day):
        self.calendar[day] = {"photos": [],
              "tracks": [],
              "stays": [],
              "bbox": {"NE_lat":90., "NE_lng":180., "SW_lat": 0., "SW_lng": 0.},
              "jsdate": p["jstime"]
              }
        
      self.calendar[day]["photos"].append(i)

    for p,i in zip(self.tracks, range(len(self.tracks))):
      p["video"] = p["video"].replace("/media/green/F0D449C8D4499230/video_sony", "video")
      p["video"] = p["video"].replace(".MP4", ".webm")
      p["thumb"] = p["video"].replace(os.path.splitext(p["video"])[1], "_thumb.jpg")
      if len(p["points"])==1: # duplicate single point to draw a fake polyline
        p["points"].append(p["points"][0])
      day = self.getDayOfTheTrip(p["endtime"][1], p["endtime"][2])
      if not self.calendar.has_key(day):
        self.calendar[day] = {"photos": [],
              "tracks": [],
              "stays": [],
              "bbox": {"NE_lat":90., "NE_lng":180., "SW_lat": 0., "SW_lng": 0.},
              "jsdate": p["endtime"]
              }
      self.calendar[day]["tracks"].append(i)
      #p["name"] = p["name"].replace("/media/green/F0D449C8D4499230/video_sony/photo", "photo_mid")

    for p,i in zip(self.stays, range(len(self.stays))):
      p["name"] = p["name"].replace("/media/green/F0D449C8D4499230/video_sony/photo", "photo")
      day = self.getDayOfTheTrip(p["checkindate"][1], p["checkindate"][2])
      if not self.calendar.has_key(day):
        self.calendar[day] = {"photos": [],
              "tracks": [],
              "stays": [],
              "bbox": {"NE_lat":90., "NE_lng":180., "SW_lat": 0., "SW_lng": 0.},
              "jsdate": p["checkindate"]
              }
      if(i > 0):
        self.calendar[day]["stays"].append(i-1)
      self.calendar[day]["stays"].append(i)

    self.assert_unique_date()
    for day_key in self.calendar:
      self.calendar[day_key]["photos"].sort(key=lambda d: self.photo_points[d]["jstime"][3]*3600+self.photo_points[d]["jstime"][4]*60+self.photo_points[d]["jstime"][5])
      self.calendar[day_key]["tracks"].sort(key=lambda d: self.tracks[d]["endtime"][3]*3600     +self.tracks[d]["endtime"][4]*60+     self.tracks[d]["endtime"][5])

    self.calculateCalendar()

    # self.tree = sp.cKDTree(np.array([(x["latitude"],x["longitude"]) for x in self.photo_points]))
    # print("%s points generated. Done!" % (len(self.photo_points)))

  def calculateCalendar(self):
    '''Compute day bbox and path'''
    print("Updating calendar...")
    for d in self.calendar:
      if d>0:
        day = self.calendar[d]
        for p_idx in day["photos"]:
          self.shrink_bbox(d, self.photo_points[p_idx]["latitude"], self.photo_points[p_idx]["longitude"])
        for p_idx in day["tracks"]:
          for point in self.tracks[p_idx]["points"]:
            self.shrink_bbox(d, point["latitude"], point["longitude"])
        for s_idx in day["stays"]:
          self.shrink_bbox(d, self.stays[s_idx]["latitude"], self.stays[s_idx]["longitude"])
        center_lat = (day["bbox"]["NE_lat"] - day["bbox"]["SW_lat"])/2.
        center_lat = day["bbox"]["SW_lat"] + center_lat
        center_lng = (day["bbox"]["NE_lng"] - day["bbox"]["SW_lng"])/2.
        center_lng = day["bbox"]["SW_lng"] + center_lng
        day["center"] = [center_lat, center_lng]

        #Fill the path coordinates in time order
        if len(day["stays"]) == 2:
          time_point_in = tuple(self.stays[d-1]["checkindate"])
          time_point_out = tuple(self.stays[d]["checkindate"])

          time_in = np.datetime64("%i-%02i-%02iT%02i:%02i:%02i.%02i" % tuple(time_point_in))
          time_out = np.datetime64("%i-%02i-%02iT%02i:%02i:%02i.%02i" % tuple(time_point_out))

          if(time_in < time_out):
            day["path"] = [ (self.stays[d-1]["latitude"],self.stays[d-1]["longitude"]),
                            (self.stays[d]["latitude"],self.stays[d]["longitude"])]
          else:
            day["path"] = []
            print("Check the day:", day)

    self.calendar["length"] = len(self.calendar.keys())

  def assert_unique_date(self):
    for day_key in self.calendar:
      d = self.calendar[day_key]
      day_n = -1
      for photo_id in d["photos"]:
        if day_n == -1:
          day_n = self.photo_points[photo_id]["jstime"][2]
        if day_n != self.photo_points[photo_id]["jstime"][2]:
          print(day_n, self.photo_points[photo_id]["jstime"][2])
          print("Achtung! Check the day" + str(self.photo_points[photo_id]))
          sys.exit()
        day_n = self.photo_points[photo_id]["jstime"][2]
      day_n = -1
      for photo_id in d["tracks"]:
        if day_n == -1:
          day_n = self.tracks[photo_id]["endtime"][2]
        if day_n != self.tracks[photo_id]["endtime"][2]:
          print(day_n, self.tracks[photo_id]["endtime"][2])
          print("Achtung! Check the day" + str(self.tracks[photo_id]))
          sys.exit()

  def shrink_bbox(self, day, lat, lng):
    if not day in [76, 81, 83, 115, 130]: #days out of the trip
      if lat == 0. or lng == 0.:
        return
      if lat < self.calendar[day]["bbox"]["NE_lat"]:
        self.calendar[day]["bbox"]["NE_lat"] = lat
      if lat > self.calendar[day]["bbox"]["SW_lat"]:
        self.calendar[day]["bbox"]["SW_lat"] = lat
      if lng < self.calendar[day]["bbox"]["NE_lng"]:
        self.calendar[day]["bbox"]["NE_lng"] = lng
      if lng > self.calendar[day]["bbox"]["SW_lng"]:
        self.calendar[day]["bbox"]["SW_lng"] = lng

  def get_photo_points(self, bb):
    ''' Lets return all the points and see if it works fast enough '''
    return self.photo_points

  def get_tracks(self, bb):
    ''' Lets return all the tracks and see if it works fast enough '''
    return self.tracks

  def get_stays(self):
    return self.stays  

  def getDayOfTheTrip(self, calendarMonth, calendarDay):
    '''Returns the day of the trip starting from the 12 Aug 2017'''
    currentDay = np.datetime64('2017-%02i-%02i' % (calendarMonth, calendarDay))
    dt = currentDay - self.firstday
    npdt = np.timedelta64(dt, 'D')
    return npdt.astype("int") + 1
  
  def get_days(self):
    #TODO: to remove redundant data
    return self.calendar

# PT()