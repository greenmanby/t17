import os
import sys
import time
import datetime
import pynmea2
if os.name == 'posix' and sys.version_info[0] < 3:
    import subprocess32 as subprocess
else:
    import subprocess
import exifread
import json
import xml.etree.ElementTree as ET

TFMT = "%Y %m %d %H %M %S"
#./gpsbabel -i nmea -f /data/video_sony/gps_logs_before_formatting/GPS/17081406.LOG -o gpx -F /data/video_sony/gps_logs_before_formatting/GPS/17081406.gpx
VIDEO_LOG_DIR = "/data/video_sony/gps_logs_before_formatting/GPS"

def nmea2gpx(input_dir):
  for root, dirs, files in os.walk(input_dir):
    for f in files:
      if(f.endswith(".LOG")):
        fullname = os.path.join(NMEA_LOG_DIR, f)
        if os.path.exists(fullname):
          cmd = ["/data/soft/gpsbabel/gpsbabel", "-i", "nmea", "-f", fullname, "-o", "gpx", "-F", fullname.replace(".LOG", ".gpx")]
          subprocess.call(cmd)

def parse_nmea():
  '''Never worked'''
  FILENAME = "/data/video_sony/gps_logs_before_formatting/GPS/17081206.LOG"
  c = 0
  with open(FILENAME, "r") as f:
    for line in f:  
      if line.startswith("$"):
        message = pynmea2.parse(line)
        print(message.fields)
        print(c)
        c = c + 1
        # except E:
        # pass 
  # sys.exit()

def export_coords_from_photos(input_dirs):
  photo_gps = []
  fileslist = []
  for d in input_dirs:
    for root, dirs, files in os.walk(d):
      for f in files:
        if(f.endswith(".jpg") or f.endswith(".JPG") or f.endswith(".jpeg")):
          fullname = os.path.join(d, f)
          if os.path.exists(fullname):
            fileslist.append(fullname)
  # print(fileslist[0], fileslist[-1])
  # sys.exit()
  c = 0
  for fullname in fileslist:
    print(fullname)

    print("%i/%i" % (c, len(fileslist)))
    f = fullname
    # if c>100:
    #   break
    c = c+1
    with open(fullname, 'rb') as photo_file:
      tags = exifread.process_file(photo_file)
      # for t in tags:
      #   print(t)
      # sys.exit()
      photo_gps_entry = { "name": f,
                          "longitude": 0.,
                          "latitude": 0.,
                          "altitude": 0.,
                          "speed": 0.,
                          "dop": 0.,
                          "jstime": []}

      for tag_name in tags:
        if tag_name == "GPS GPSLatitude":
          s = [x.num/float(x.den) for x in list(tags[tag_name].values)]
          photo_gps_entry["latitude"] = s[0]+(s[1]+s[2]/60.)/60.
        elif tag_name == "GPS GPSLongitude":
          s = [x.num/float(x.den) for x in list(tags[tag_name].values)]
          photo_gps_entry["longitude"] = s[0]+(s[1]+s[2]/60.)/60.
        elif tag_name == "GPS GPSAltitude":
          photo_gps_entry["altitude"] = [x.num/float(x.den) for x in list(tags[tag_name].values)][0]
        elif tag_name == "GPS GPSSpeed":
          photo_gps_entry["speed"] = [x.num/float(x.den) for x in list(tags[tag_name].values)][0]
        elif tag_name == "GPS GPSDOP":
          photo_gps_entry["dop"] = [x.num/float(x.den) for x in list(tags[tag_name].values)][0]
        elif tag_name == "GPS GPSStatus":
          photo_gps_entry["gps_status"] = str(tags[tag_name].values)
        # elif tag_name == "GPS GPSDate":
        #   photo_gps_entry["gps_date"] = str(tags[tag_name].values)
        # elif tag_name == "GPS GPSTimeStamp":
        #   photo_gps_entry["gps_timestamp"] = str(tags[tag_name].values)
        # elif tag_name == "EXIF DateTimeOriginal":
        #   photo_gps_entry["exif_date"] = str(tags[tag_name].values)
        
      jstime = time.strftime(TFMT, time.localtime(os.path.getmtime(f)))
      photo_gps_entry["jstime"] = map(int, jstime.split())+[0]

      photo_gps.append(photo_gps_entry)
  return photo_gps

def export_coords_from_gpx(input_dir):
  video_gps = []
  fileslist = []
  for root, dirs, files in os.walk(input_dir):
    for f in files:
      if(f.endswith(".gpx")):
        fullname = os.path.join(input_dir, f)
        if os.path.exists(fullname):
          fileslist.append(fullname)
  c = 0
  with open("gpx_match.json", "r") as gpx_match_file:
    gpx_match = json.loads(gpx_match_file.read())

    for fullname in fileslist:
      print("%i/%i %s" % (c, len(fileslist), fullname))
      track = { "name": fullname,
                "video": "",
                "points" : [],
                "bbox" : [],
                "endtime" : []}
      isVideoFound = False
      if gpx_match.has_key(fullname):
        track["video"] = gpx_match[fullname]
        video_time = time.strftime(TFMT, time.localtime(os.path.getmtime(gpx_match[fullname])))
        track["endtime"] = map(int, video_time.split())+[0]
        isVideoFound = True
      else:
        #TODO: find out why 3 videos are probably are missing
        pass
      # if c>10:
      #   break
      c = c+1

      tree = ET.parse(fullname)
      for child in tree.getroot():
        track_found = False
        if(child.tag == "{http://www.topografix.com/GPX/1/0}bounds"):
          pass
        if(child.tag == "{http://www.topografix.com/GPX/1/0}trk"):
          track_found = True
          track_starttime = datetime.datetime.now()
          track_endtime = datetime.datetime(1900, 01, 01)

          for point in child[0]:
            gps_track_point = { "latitude": 0.,
                                "longitude": 0.,
                                "altitude": 0.,
                                "speed": 0.,
                              }
            gps_track_point["latitude"] = float(point.get("lat"))
            gps_track_point["longitude"] = float(point.get("lon"))
            gps_track_point["speed"] = float(point.find("{http://www.topografix.com/GPX/1/0}speed").text)
            gps_track_point["altitude"] = float(point.find("{http://www.topografix.com/GPX/1/0}ele").text)
            track["points"].append(gps_track_point)
            if point == child[0][-1] and not isVideoFound:
              point_time = point.find("{http://www.topografix.com/GPX/1/0}time").text
              try:
                point_dtime = datetime.datetime.strptime(point_time, "%Y-%m-%dT%H:%M:%S.%fZ")
              except ValueError as e:
                try:
                  point_dtime = datetime.datetime.strptime(point_time, "%Y-%m-%dT%H:%M:%SZ")
                except ValueError as e:
                  print(e)
                  raise
              track["endtime"] = map(int, point_dtime.strftime(TFMT).split())

          NE_lat = 90.
          NE_lng = 180.
          SW_lat = 0.
          SW_lng = 0.
          for p in track["points"]:
            if p["latitude"] < NE_lat:
              NE_lat = p["latitude"]
            if p["latitude"] > SW_lat:
              SW_lat = p["latitude"]
            if p["longitude"] < NE_lng:
              NE_lng = p["longitude"]
            if p["longitude"] > SW_lng:
              SW_lng = p["longitude"]
          track["bbox"] = [NE_lat, NE_lng, SW_lat, SW_lng]
          video_gps.append(track)
  return video_gps

GPX_TRACKS = "/media/green/F0D449C8D4499230/video_sony/gps_logs_before_formatting/GPS"
# video_gps_tracks = export_coords_from_gpx(GPX_TRACKS)
# with open('videos.json', 'w') as f:
#    json.dump(video_gps_tracks, f, indent=2)

CAMERA_PHOTOS = "/media/green/F0D449C8D4499230/video_sony/photo/camera"
PHONE_PHOTOS = "/media/green/F0D449C8D4499230/video_sony/photo/phone"
STG_PHOTOS = "/media/green/F0D449C8D4499230/video_sony/photo/stg"
# photo_data = export_coords_from_photos((STG_PHOTOS, PHONE_PHOTOS, CAMERA_PHOTOS))
# with open('photos.json', 'w') as f:
#   json.dump(photo_data, f, indent=2)


def generate_image_thumbs(input_dirs):
  for inputDir in input_dirs:
    for root, dirs, files in os.walk(inputDir):
      for f in files:
        if(f.endswith(".jpg") or f.endswith(".JPG") or f.endswith(".jpeg")):
          fullname = os.path.join(inputDir, f)
          if os.path.exists(fullname):
            print(fullname)
            newname = fullname.replace("photo", "photo_mid")
            newname = newname.replace(os.path.splitext(newname)[1], "_thumb"+os.path.splitext(newname)[1])
            cmd = ["/usr/bin/convert", fullname, "-resize", "160>", newname]
            subprocess.call(cmd)
# generate_image_thumbs([CAMERA_PHOTOS, PHONE_PHOTOS, STG_PHOTOS])

def generate_video_thumbs():
  VID = r"/media/green/F0D449C8D4499230/video_sony"
  fileslist = []
  for root, dirs, files in os.walk(VID):
    for f in files:
      if f.endswith(".jpg") and not f.endswith("_thumb.jpg"):
        fullname = os.path.join(VID, f)
        if os.path.exists(fullname):
          fileslist.append(fullname)
  for f in fileslist:
    print(f)
    newname = f.replace(os.path.splitext(f)[1], "_thumb.jpg")
    cmd = ["/usr/bin/convert", f, "-resize", "160>", "-quality", "99", newname]
    subprocess.call(cmd)
    cmd = ["composite", "-gravity", "center", "/media/green/F0D449C8D4499230/video_sony/play.png", newname, newname]
    subprocess.call(cmd)
# generate_video_thumbs()