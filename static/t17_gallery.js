function T17Gallery(t17map){
  this.t17map = t17map;
  this.maxNumberToShow = 50;

  this.drawPhotos = true;
  this.drawVideos = true;
  this.drawStays = true;

  $(".filter-checkbox").change($.proxy(function() {
    this.drawPhotos = $("#show-photos").prop('checked');
    this.drawVideos = $("#show-videos").prop('checked');
    this.drawStays = $("#show-stays").prop('checked');
    this.t17map.redrawData();
  }, this));

  this.setData = function(data){
    this.photos = data.photo_points;
  }

  this.addPic = function(dataId, type) {
    var imageGalleryItem = document.createElement('div');
    imageGalleryItem.setAttribute("class", "image-gallery-item");
    var link = document.createElement('a');
    var pic = document.createElement('img');
    imageGalleryItem.append(link);
    link.append(pic);

    if(type == "photo"){
      photo_point = this.t17map.data.photo_points[dataId];
      link.setAttribute("href", 'static/'+photo_point.name);
      link.setAttribute("title", 'Super photo');
      link.setAttribute("class", 'popup-gallery');
      pic.setAttribute("src", 'static/'+photo_point.thumb);
      pic.setAttribute("alt", 'static/'+photo_point.name);
      pic.setAttribute("class", 'galleryEntry');
      $('#image-gallery').append(imageGalleryItem);

      pic.onmouseover = $.proxy(function(){
          this.pStyles.highlightPhotoById(dataId);
        },
        this.t17map);
      pic.onmouseout = $.proxy(function(){
          this.pStyles.resetPhotoStyleById(dataId);
        },
        this.t17map);
    }
    else if(type == "video"){
      track = this.t17map.data.tracks[dataId];
      if(track.video){
        var videoLink = 'static/'+track.video;
        link.setAttribute("href", videoLink);
        link.setAttribute("id", "video_"+dataId);
        pic.setAttribute("src", 'static/'+track.thumb);
        pic.setAttribute("alt", videoLink);
        pic.setAttribute("class", "galleryEntry");
        $('#video-gallery').append(link);

        $('#video_'+dataId).magnificPopup({
          items: {
                  src: '<div class="popup-video-content">'+
                        '<video controls muted="True"><source src='+videoLink+'></video>'+
                        '</div>',
                  type: 'inline',
          },
          midClick: true,
          closeBtnInside: true
        });

        pic.onmouseover = $.proxy(function(){
            this.pStyles.highlightVideoById(dataId);
          },
          this.t17map);
        pic.onmouseout = $.proxy(function(){
            this.pStyles.resetVideoStyleById(dataId);
          },
          this.t17map);
      }
      else{
        // console.log(track);
      }
    }
  };

  this.redraw = function(photoIds, videoIds, maxNumberToShow){
    this.photoIds = photoIds;
    this.videoIds = videoIds;
    $('#image-gallery').empty();
    $('#video-gallery').empty();

    if(this.t17map.data){
      if(this.drawPhotos){
        for(var i=0; i<this.photoIds.length && i < maxNumberToShow; ++i){
          this.addPic(this.photoIds[i], "photo");
        }
      }
      if(this.drawVideos){
        for(var i=0; i<this.videoIds.length && i < maxNumberToShow; ++i){
          this.addPic(this.videoIds[i], "video");
        }
      }
      if(this.photoIds.length > maxNumberToShow || this.videoIds.length > maxNumberToShow){
        numItemsUnloaded = Math.abs(this.photoIds.length-maxNumberToShow) + Math.abs(this.videoIds.length-maxNumberToShow);
        $("#button-more").show();
        $("#button-more").text("Load remaining media: "+ numItemsUnloaded + " items");
      }
      else{
        $("#button-more").hide();
      }

      $('#image-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
          enabled: true,
          navigateByImgClick: true,
          preload: [0,1] // Will preload 0 - before current, and 1 after the current image
        },
        image: {
          tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
          titleSrc: function(item) {
            return item.el.attr('title') + '<small>somewhere</small>';
          }
        }
      });
    }
  }

  $("#button-more").click($.proxy(function(){
    this.redraw(this.photoIds, this.videoIds, 10000); // Should be enough for everyone
  },
  this));
}
