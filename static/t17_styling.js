function T17Styles(t17map){
  this.t17map = t17map;

  this.pointStyle_default = {};
  this.pointStyle_selected = {};
  this.trackStyle_default = {};
  this.trackStyle_selected = {};
  this.stayStyle_default = {};
  this.stayStyle_selected = {};
  this.dayPathStyle_default = {color: '#404040', weight: 2., opacity: 0.2};
  this.dayPathStyle_selected = {color: '#a04040', weight: 7., opacity: 0.4};
  this.connectorStyle_default = {color: '#33cc33', weight: 2., opacity: 0.5};
  this.updateStyles = function(){
    var selColor = '#30dddd';
    var defaultRadius = 1500000./Math.pow(2,(this.t17map.zoomLevel+1));
    var selectedRadius = 2500000./Math.pow(2,(this.t17map.zoomLevel+1));
    this.pointStyle_default = {fillColor: '#33cc33', stroke: true, color: '#04b0a6', weight: 1., radius: defaultRadius, fillOpacity: 0.6};
    this.pointStyle_selected = {fillColor: selColor, stroke: true, color: '#04b0a6', weight: 2., radius: selectedRadius, fillOpacity: 10};
    this.trackStyle_default = {color: '#a00505', weight: 10.};
    this.trackStyle_selected = {color: selColor, weight: 25.};
    this.stayStyle_default = {fillColor: '#431086', stroke: false, radius: defaultRadius, fillOpacity: 0.9 };
    this.stayStyle_selected = {fillColor: selColor, stroke: true, color: '#330066', radius: selectedRadius, fillOpacity: 1.0};
  }

  this.setPointSelectedStyle = function(e, optionalRadiusFactor = 1.){
    e.target.setRadius(optionalRadiusFactor * this.pointStyle_selected["radius"]);
    e.target.setStyle(this.pointStyle_selected);
  }

  this.setPointDefaultStyle = function(e,){
    e.target.setRadius(this.pointStyle_default["radius"]);
    e.target.setStyle(this.pointStyle_default);
  }

  this.setTrackSelectedStyle = function(e, optionalWeightFactor = 1.){
    var tmpStyle = Object.assign({}, this.trackStyle_selected); //copy object
    $.extend(tmpStyle, {weight:this.trackStyle_selected.weight * optionalWeightFactor});
    e.target.setStyle(tmpStyle);
  }

  this.setTrackDefaultStyle = function(e){
    e.target.setStyle(this.trackStyle_default); 
  }

  this.setStaySelectedStyle = function(e){
    e.target.setRadius(this.stayStyle_selected["radius"]);
    e.target.setStyle(this.stayStyle_selected);
  }

  this.setStayDefaultStyle = function(e){
    e.target.setRadius(this.stayStyle_default["radius"]);
    e.target.setStyle(this.stayStyle_default);
  }

  this.highlightPhotoById = function(id){
    this.setPointSelectedStyle({target: this.t17map.data.photo_points[id].marker}, 2.);
  }
  this.resetPhotoStyleById = function(id){
    this.setPointDefaultStyle({target: this.t17map.data.photo_points[id].marker});
  }

  this.highlightVideoById = function(id){
    this.setTrackSelectedStyle({target: this.t17map.data.tracks[id].marker}, 2.);
  }
  this.resetVideoStyleById = function(id){
    this.setTrackDefaultStyle({target: this.t17map.data.tracks[id].marker});
  }
}