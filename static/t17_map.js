function T17Map(){
  this.mapHandler;
  this.data;
  this.dataGroup = L.layerGroup();
  this.bgGroup = L.layerGroup();
  this.zoomLevel = 7;

  this.gallery = new T17Gallery(this);
  this.timeline = new T17Timeline(this);
  this.pStyles = new T17Styles(this);
  this.day = 0;

  L.mapquest.key = 'B0hL5fEWeACvpS9AKkfVtnMUhe3EHWHS';
  this.mapHandler = L.mapquest.map('map', {
      center: [39.442830, 48.547452],
      layers: L.mapquest.tileLayer('map'),
      zoom: this.zoomLevel,
      zoomsnap: 0.1
    });
  this.bgGroup.addTo(this.mapHandler);
  this.dataGroup.addTo(this.mapHandler);
  this.mapHandler.on("moveend", $.proxy(function(){this.redrawData();},this));
  this.mapHandler.on("zoomend", $.proxy(function(){
      this.zoomLevel = this.mapHandler.getZoom();
      this.pStyles.updateStyles();
    },
    this)
  );

  this.getData = function() {
    $("#map-container").hide();
    $("#loading-placeholder").show();
    $.getJSON('/_get_visible',
              {/*args*/},
              function(data){this.dataArrived(data);}.bind(this)
              );
  };
  this.mapHandler.whenReady($.proxy(function(){
    $("#show-photos").prop('checked', true);
    $("#show-videos").prop('checked', true);
    $("#show-stays").prop('checked', true);
    this.gallery.drawPhotos = true;
    this.gallery.drawVideos = true;
    this.gallery.drawStays = true;

    this.getData();
  },this));

  this.setDay = function(day){
    this.day = day;
    
    for(var i=1; i<this.data.days.length; ++i){
      if(this.data.days[i]){
        if(this.data.days[i].marker){
          this.data.days[i].marker.remove();
        }
      }
    }

    if(day!=0){
      var dayData = this.data.days[day];
      var dateValue = this.data.days[day].jsdate[2]+" / "+this.data.days[day].jsdate[1]+" / "+this.data.days[day].jsdate[0]
      $("#date").empty();
      $("#date").append(dateValue);

        
      if(dayData && dayData.path){
        var latlngs = [];
        for(var j=0; j<dayData.path.length; ++j){
            latlngs.push([dayData.path[j][0], dayData.path[j][1]]);
        }
        dayData.marker = L.polyline(latlngs, this.pStyles.dayPathStyle_selected);
        dayData.marker.addTo(this.bgGroup);
      }

        var NE = L.latLng(dayData.bbox.NE_lat, dayData.bbox.NE_lng);
        var SW = L.latLng(dayData.bbox.SW_lat, dayData.bbox.SW_lng);
        var dayBBox = L.latLngBounds(NE, SW);
        var dayZoom = this.mapHandler.getBoundsZoom(dayBBox);
      this.mapHandler.setView([dayData.center[0], dayData.center[1]], dayZoom);
    }
    else{
      //reset to the whole view
      $("#date").empty();
      $("#date").append("Date: -  ");
        var NE = L.latLng(55., 80.);
        var SW = L.latLng(30., 7.);
        var dayBBox = L.latLngBounds(NE, SW);
        var dayZoom = this.mapHandler.getBoundsZoom(dayBBox);
      this.mapHandler.setView(dayBBox.getCenter(), dayZoom);
    }
  }

  this.dataArrived = function(dataFormServer){
    this.data = dataFormServer.result;
    this.setDay(0);
    this.gallery.setData(this.data);
    this.drawPath();
    $("#loading-placeholder").hide();
    $("#map-container").show();
  }

  this.drawPath = function(){
    for (var i = 0; i < this.data.days.length; i++) {
      var day = this.data.days[i];
      if(day && day.path){
        var latlngs = [];
        for(var j=0; j<day.path.length; ++j){
            latlngs.push([day.path[j][0], day.path[j][1]]);
        }
        this.data.days[i].bgmarker = L.polyline(latlngs, this.pStyles.dayPathStyle_default);
        day.bgmarker.addTo(this.bgGroup);
      }
    }
  }

  this.redrawData = function(){
    //TODO: add map scale primitive

    this.dataGroup.clearLayers();
    var mapBbox = this.mapHandler.getBounds();
    var visibleVideoIds = [];
    var visiblePointsIds = [];
    var popupStyle = {autoPanPadding: true,
                      autoPanPadding: L.point(50,50),
                      maxWidth: "auto",
                      keepInView: true,
                      autoClose: false};

    //draw video tracks
    if(this.gallery.drawVideos){
      for (var i = 0; i < this.data.tracks.length; i++) {
        var track = this.data.tracks[i];
        var track_bbox = L.latLngBounds(L.latLng(track.bbox[0], track.bbox[1]), 
                                        L.latLng(track.bbox[2], track.bbox[3]));
        
        var isVisible = false;
        if(this.day != 0){  
          var isIdInDay = $.inArray( i, this.data.days[this.day].tracks );
          if (isIdInDay != -1){
            isVisible = true;
          }
        }
        else{
          if(mapBbox.intersects(track_bbox)){
            isVisible = true;
          }
        }
        if(isVisible){
          visibleVideoIds.push(i);

          var latlngs = [];
          for(var j=0; j<track.points.length; ++j){
            latlngs.push([track.points[j].latitude, track.points[j].longitude]);
          }
          this.data.tracks[i].marker = L.polyline(latlngs, this.pStyles.trackStyle_default);

          if(track.video){
            var popup = L.popup(popupStyle);
            popup.setContent("<video width=480 controls muted='True'><source src='/static/"+track.video+"'></video>");
            //TODO: check if the video file is available. If not - replace with non-interactive path

            track.marker.bindPopup(popup);

            track.marker.on('mouseover', $.proxy(function(e){this.pStyles.setTrackSelectedStyle(e);},this));
            track.marker.on('mouseout', $.proxy(function(e){
              this.pStyles.setTrackDefaultStyle(e);
              // setTimeout(function() { e.target.closePopup(); }, 2000);
            },this));
          }
          track.marker.addTo(this.dataGroup);
        }
      }
    }

    //draw overnight locations
    if(this.gallery.drawStays){
      for (var i = 0; i < this.data.stays.length; i++) {
        var entry = this.data.stays[i];
        var position = [entry.latitude, entry.longitude];
        if(mapBbox.contains(position)){
          var overnightPoint = L.circle(position, this.pStyles.stayStyle_default);
          
          var popup = L.popup(popupStyle);
          popup.setContent(entry.name+"<br>"+entry.desc);
          overnightPoint.bindPopup(popup);

          overnightPoint.on('mouseover', $.proxy(function(e){
            this.pStyles.setStaySelectedStyle(e);
            setTimeout(function() { e.target.openPopup(); }, 300);
            
          },this));
          overnightPoint.on('mouseout', $.proxy(function(e){
            this.pStyles.setStayDefaultStyle(e);
            setTimeout(function() { e.target.closePopup(); }, 400);
          },this));
          overnightPoint.addTo(this.dataGroup);
        }
      }
    }

    //draw photos points
    if(this.gallery.drawPhotos){
      //Calculate photo pins radius
      var dr = 0.;
      if(this.zoomLevel > 4){
        dr = 40.;
        if(this.zoomLevel < 9){
          dr = dr *  (0.3 * this.zoomLevel - 1.2);
        }
      }
      dr = dr / Math.pow(2,(this.zoomLevel+1));

      for (var i = 0; i < this.data.photo_points.length; i++) {
        var entry = this.data.photo_points[i];

        var pos = [entry.latitude, entry.longitude];
        var isVisible = false;
        if(this.day != 0){  
          var isIdInDay = $.inArray( i, this.data.days[this.day].photos );
          if (isIdInDay != -1){
            isVisible = true;
          }
        }
        else{
          if(mapBbox.contains(pos)){
            isVisible = true;
          }
        }
        if(isVisible){
          visiblePointsIds.push(i);
          
          var rndGen = SeedRandom(i);
          var a_rand01 = rndGen(500) / 500.; //don't need more than this limit
          var b_rand01 = rndGen(500) / 500.; //don't need more than this limit
          var radius = dr + dr*0.5*(b_rand01-1.)*2.;
          var phi = a_rand01 * 2.*Math.PI;
          var drawPos = [pos[0] + radius*Math.cos(phi), pos[1]+ radius*Math.sin(phi)];

          this.data.photo_points[i].marker = L.circle(drawPos, this.pStyles.pointStyle_default);
          var connector = L.polyline([pos, drawPos], this.pStyles.connectorStyle_default);

          connector.addTo(this.dataGroup);
          entry.marker.addTo(this.dataGroup);

          var popup = L.popup(popupStyle);
          popup.setContent("<a href='/static/"+entry.name+"'+ target='_blank'><img src='/static/"+entry.thumb+"' alt="+entry.name+"></a>");
          entry.marker.bindPopup(popup);

          entry.marker.on('mouseover', $.proxy(function(e){this.pStyles.setPointSelectedStyle(e);},this));
          entry.marker.on('mouseout', $.proxy(function(e){this.pStyles.setPointDefaultStyle(e);},this));   
        }
      }
    }

    this.gallery.redraw(visiblePointsIds, visibleVideoIds, this.gallery.maxNumberToShow);   
  }


  this.drawAltitudeGraph = function() {
    // var getElevationProfileData = "http://open.mapquestapi.com/elevation/v1/profile?key=B0hL5fEWeACvpS9AKkfVtnMUhe3EHWHS&shapeFormat=raw&latLngCollection=39.74012,-104.9849,39.7995,-105.7237,39.6404,-106.3736";
    var getElevationGraph = "http://open.mapquestapi.com/elevation/v1/chart?key=B0hL5fEWeACvpS9AKkfVtnMUhe3EHWHS&shapeFormat=raw&width=800&height=300&latLngCollection=39.74012,-104.9849,39.7995,-105.7237,39.6404,-106.3736";
    var elevationGraph = document.createElement('img');
    elevationGraph.setAttribute("src", getElevationGraph);
    document.body.append(elevationGraph);
  }
}

