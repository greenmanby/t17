function T17Timeline(t17map){
  this.t17map = t17map;

  this.setSliderTicks = function(el) {
    var $slider =  $('#timeline-slider');
    var max =  $slider.slider("option", "max");    
    var spacing =  100 / (max -1);

    $slider.find('.ui-slider-tick-mark').remove();
    for (var i = 0; i < max ; i++) {
      $('<span class="ui-slider-tick-mark"></span>').css('left', (spacing * i) +  '%').appendTo($slider); 
     }
  }

  this.timeline = $("#timeline-slider").slider({ value:1, min: 1, max: 70, step: 1, orientation: "horizontal",
                                                  create: $.proxy(function( event, ui ) { this.setSliderTicks(event.target); }, this)
                                                });

  this.timeline.on("slide", $.proxy(function(event, ui){
    $("#number").empty();
    $("#number").append("Day: "+ui.value);
    this.setDay(ui.value);
  },
  this.t17map));

  $("#button-next").click( $.proxy(function(){
    var newVal = this.day;
    if(newVal < $("#timeline-slider").slider("option", "max")){
      newVal += 1;
    }
    $("#number").empty();
    $("#number").append("Day: "+newVal);
    $("#timeline-slider").slider( "value", newVal );
    this.setDay(newVal);
  },
  this.t17map));

  $("#button-previous").click( $.proxy(function(){
    var newVal = this.day;
    if(newVal > $("#timeline-slider").slider("option", "min")){
      newVal -= 1;
    }
    $("#number").empty();
    $("#number").append("Day: "+newVal);
    $("#timeline-slider").slider( "value", newVal );
    this.setDay(newVal);
  },
  this.t17map));

  $("#button-reset").click( $.proxy(function(){
    $("#number").empty();
    $("#number").append("Day: -,");
    $("#timeline-slider").slider( "value", 1 );
    this.setDay(0);
  },
  this.t17map));

}

