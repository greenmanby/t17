from flask import Flask, render_template, request, jsonify, send_from_directory
import prepare_points

app = Flask(__name__)

app.config['LEAFLETDIST'] = 'node_modules/leaflet/dist'
app.config['LEAFLETMARKERS'] = 'node_modules/leaflet.markercluster/dist'


pt = prepare_points.PT()

@app.route('/_get_visible')
def get_visible():
    NE_lat = None #request.args.get('NE_lat', 0, type=float)
    NE_lng = None #request.args.get('NE_lng', 0, type=float)
    SW_lat = None #request.args.get('SW_lat', 0, type=float)
    SW_lng = None #request.args.get('SW_lng', 0, type=float)
    screen_bbox = (NE_lat, NE_lng, SW_lat, SW_lng)
    photo_points = pt.get_photo_points(screen_bbox)
    tracks = pt.get_tracks(screen_bbox)
    stays = pt.get_stays()
    days = pt.get_days()
    return jsonify(result={"photo_points": photo_points, "tracks": tracks, "stays": stays, "days": days})


# Custom static data
@app.route('/<path:filename>')
def custom_static(filename):
    return send_from_directory("", filename)


@app.route('/')
def main():
    # db = get_db()
    # cur = db.execute('select title, text from entries order by id desc')
    # entries = cur.fetchall()
    #return render_template('show_entries.html', entries=entries)
    input = {   
                "name":"boo",
                "w": 320,
                "h": 180
            }
    return render_template("index.html", input_data=input)

if __name__ == "__main__":
    app.run()