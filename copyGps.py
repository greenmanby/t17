#!/usr/bin/python
import os
import sys
if os.name == 'posix' and sys.version_info[0] < 3:
    import subprocess32 as subprocess
else:
    import subprocess
import exifread


def degToDMS(dec):
  deg = int(dec)
  mn_dec = (dec - deg)*60
  mn = int(mn_dec)
  sec = (mn_dec - mn)*60
  return deg, mn, sec


def getLatLng(filename):
  lat = 0.
  lng = 0.
  with open(filename, 'rb') as f:
    tags = exifread.process_file(f)
    for tag_name in tags:
      if tag_name == "GPS GPSLongitude":
        s = [x.num/float(x.den) for x in list(tags[tag_name].values)]
        lng = s[0]+(s[1]+s[2]/60.)/60.
      elif tag_name == "GPS GPSLatitude":
        s = [x.num/float(x.den) for x in list(tags[tag_name].values)]
        lat = s[0]+(s[1]+s[2]/60.)/60.
  return (lat, lng)


def setLatLng(filename, lat, lng):
  # cmd = ["/usr/bin/exiftool", "-GPSLatitude=%i,%i,%f" % degToDMS(float(sys.argv[2])), sys.argv[1]]
  # subprocess.call(cmd)
  # cmd = ["/usr/bin/exiftool", "-GPSLongitude=%i,%i,%f" % degToDMS(float(sys.argv[3])), sys.argv[1]]
  # subprocess.call(cmd)
  cmd = ["/usr/bin/exiftool", "-GPSLatitude=%i,%i,%f" % degToDMS(lat), filename]
  subprocess.call(cmd)
  cmd = ["/usr/bin/exiftool", "-GPSLongitude=%i,%i,%f" % degToDMS(lng), filename]
  subprocess.call(cmd)


def checkInput(filename):
  if not (filename.endswith("jpg") or filename.endswith("JPG") or filename.endswith("jpeg")) or not os.path.exists(filename):
    print("Input error: %s" % (filename))
    sys.exit()
  print("Input %s OK." % (filename))


def checkOutput(filename):
  if not (filename.endswith("jpg") or filename.endswith("JPG") or filename.endswith("jpeg")) or not os.path.exists(filename):
    print("Output error: %s" % (filename))
    sys.exit()

  lat, lng = getLatLng(filename)
  if not (lat == 0. or lng == 0.):
    print("Output coords aren't zero!: %s, [%f, %f]" % (filename, lat, lng))
    sys.exit()
  else:
    print("Empty coords. OK.")
  print("Output %s OK." % (filename))


def setGps(inputFile, outputFile, inLat = None, inLng = None):
  if not inLat and not inLng:
    inLat, inLng = getLatLng(inputFile)
  mtime = os.path.getmtime(outputFile)
  atime = os.path.getatime(outputFile)

  #Set coordinates
  setLatLng(outputFile, inLat, inLng)

  #Restore original modidfication time
  os.utime(outputFile, (atime, mtime))

if len(sys.argv)>2:
  if (sys.argv[1] == "--mtime"):
    mtime_src = os.path.getmtime(sys.argv[2])
    atime_src = os.path.getatime(sys.argv[2])
    os.utime(sys.argv[3], (atime_src, mtime_src))
    sys.exit()

if len(sys.argv)==3:
  '''Cmd utility'''
  
  inputFile = sys.argv[1]
  checkInput(inputFile)

  outputFile = sys.argv[2]
  checkOutput(outputFile)

  setGps(inputFile, outputFile)

elif len(sys.argv)==4:
  '''Cmd utility'''

  outputFile = sys.argv[1]
  checkOutput(outputFile)

  lat = float(sys.argv[2][:-1])
  lng = float(sys.argv[3])
  setGps("", outputFile, lat, lng)

else:
  INDIR = r"/data/projects/t17/photos_no_gps.list"
  fileslist = []
  with open(INDIR, "r") as f:
    fileslist = [x[0:-1] for x in f.readlines()]
  for filename in fileslist:
    print(filename)
    checkOutput(filename)
    mtime_dest = os.path.getmtime(filename)

    num = filename[filename.find("DSC")+3:filename.find("DSC")+8]
    cnt = int(num)+1
    nextFile = filename.replace(num, "%05i" % (cnt))
    while not os.path.exists(nextFile):
      ncnt = cnt + 1
      nextFile = nextFile.replace(str(cnt), "%05i" % (ncnt))
      if(cnt > 100):
        print("Nachbar wird nicht gefunden")
        break
    mtime_src = os.path.getmtime(nextFile)
    if abs(mtime_src - mtime_dest) < 15:
      inputFile = nextFile
      checkInput(inputFile)

      setGps(inputFile, filename)

