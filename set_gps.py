#!/usr/bin/python
import os
import sys
import time
import datetime
import json
import xml.etree.ElementTree as ET

if os.name == 'posix' and sys.version_info[0] < 3:
    import subprocess32 as subprocess
else:
    import subprocess
import exifread

TFMT = "%Y %m %d %H %M %S"

def exec_exif(input_dir):
  for root, dirs, files in os.walk(input_dir):
    for f in files:
      if(f.endswith(".LOG")):
        fullname = os.path.join(NMEA_LOG_DIR, f)
        if os.path.exists(fullname):
          cmd = ["/data/soft/gpsbabel/gpsbabel", "-i", "nmea", "-f", fullname, "-o", "gpx", "-F", fullname.replace(".LOG", ".gpx")]
          subprocess.call(cmd)


def doSomethingWithEXIFcoordinates():
  directory = r"/media/green/F0D449C8D4499230/video_sony/photo/phone/"
  fileslist = []
  for root, dirs, files in os.walk(directory):
    for f in files:
      if(f.endswith(".jpg") or f.endswith(".JPG") or f.endswith(".jpeg")):
        fullname = os.path.join(directory, f)
        if os.path.exists(fullname):
          fileslist.append(fullname)

  for f in fileslist:
    lat = 0.
    lng = 0.
    with open(f, 'rb') as photo_file:
      tags = exifread.process_file(photo_file)
      for tag_name in tags:
        if tag_name == "GPS GPSLongitude":
          s = [x.num/float(x.den) for x in list(tags[tag_name].values)]
          lng = s[0]+(s[1]+s[2]/60.)/60.
        elif tag_name == "GPS GPSLatitude":
          s = [x.num/float(x.den) for x in list(tags[tag_name].values)]
          lat = s[0]+(s[1]+s[2]/60.)/60.
      if lat == 0. or lng == 0.:
        print(f)
        # print("Empty coordinates")
        # sys.exit()
# doSomethingWithEXIFcoordinates()


def restoreSTGdates():
  STG = r"/media/green/F0D449C8D4499230/video_sony/photo/stg/"
  fileslist = []
  for root, dirs, files in os.walk(STG):
    for f in files:
      if(f.endswith(".jpg")):
        fullname = os.path.join(STG, f)
        if os.path.exists(fullname):
          fileslist.append(fullname)

  for f in fileslist:
    '''Restore STG dates'''
    orig = f.replace(os.path.splitext(f)[1], os.path.splitext(f)[1]+"_original")
    if(os.path.exists(orig)):
      mtime = os.path.getmtime(orig)
      atime = os.path.getatime(orig)
      os.utime(f, (atime, mtime))
    else:
      print("Original file is missing")
# restoreSTGdates()

def convert_overnight_date():
  overnights = []
  with open("overnight.json", "r") as f:
    overnights = json.loads(f.read())
  for stay in overnights:
    orig = stay.pop("date")
    tfmt = "%d.%m.%y %H:%M"
    checkin = datetime.datetime.strptime(orig+" 23:59", tfmt)
    checkout = datetime.datetime.strptime(orig+" 00:01", tfmt)
    # checkout += datetime.timedelta(days=1)
    checkin = checkin.strftime("%Y %m %d %H %M %S")
    stay["checkindate"] = map(int, checkin.split())+[0]
  with open("overnight.json", "w") as f:
    json.dump(overnights, f, indent=2)


def map_gpx_video(dumpfile, gpxdir, viddir):
  
  gpx_fileslist = {}
  for root, dirs, files in os.walk(gpxdir):
    for f in files:
      if f.endswith(".gpx"):
        fullname = os.path.join(gpxdir, f)
        gpx_fileslist[fullname] = 0.0

  vid_fileslist = {}
  for root, dirs, files in os.walk(viddir):
    for f in files:
      if f.endswith(".MP4"):
        fullname = os.path.join(viddir, f)
        vid_fileslist[fullname] = 0.0

  for vidfile in vid_fileslist:
    mtime = os.path.getmtime(vidfile)
    vid_fileslist[vidfile] = mtime

  for gpxfile in gpx_fileslist:
    # print(gpxfile)
    tree = ET.parse(gpxfile)
    for child in tree.getroot():
      track_found = False
      if(child.tag == "{http://www.topografix.com/GPX/1/0}bounds"):
        pass
      if(child.tag == "{http://www.topografix.com/GPX/1/0}trk"):
        track_found = True
        track_starttime = datetime.datetime.now()
        track_endtime = datetime.datetime(1900, 01, 01)
        
        for point in child[0]:
          point_time = point.find("{http://www.topografix.com/GPX/1/0}time").text
          try:
            point_dtime = datetime.datetime.strptime(point_time, "%Y-%m-%dT%H:%M:%S.%fZ")
          except ValueError as e:
            try:
              point_dtime = datetime.datetime.strptime(point_time, "%Y-%m-%dT%H:%M:%SZ")
            except ValueError as e:
              print(e)
              raise
          if(point_dtime < track_starttime):
            track_starttime = point_dtime
          if(point_dtime > track_endtime):
            track_endtime = point_dtime
        end_unix = time.mktime(track_endtime.timetuple())
        gpx_fileslist[gpxfile] = end_unix
  

  with open(dumpfile, 'w') as f:
    json.dump((gpx_fileslist, vid_fileslist), f)


def find_videos_for_gpx():
  GPX = "/media/green/F0D449C8D4499230/video_sony/gps_logs_before_formatting/GPS"
  VID = "/media/green/F0D449C8D4499230/video_sony"

  dumpfile = "gpx_vid.json"
  if not os.path.exists(dumpfile):
    map_gpx_video(dumpfile, GPX, VID)
  with open(dumpfile, 'r') as f:
    gpx,vid = json.loads(f.read())

  gpx_full = list(gpx.keys())
  gpx_short = []
  gpx_map = {}


  for vid_file in vid:
    tv = vid[vid_file]
    match_found = False
    for g in gpx:
      track_t = gpx[g]
      #I don't know the timezone (or don't want to get it form coordinates and date)
      #That's why check all the hours offset possible
      ts = [  track_t,
              track_t + 3600. * 1,
              track_t + 3600. * 2,
              track_t + 3600. * 3,
              track_t + 3600. * 4,
              track_t + 3600. * 4.5, #for Iranian weirdos
              track_t + 3600. * 5,
              track_t + 3600. * 6,
              track_t + 3600. * 7,
              track_t + 3600. * 8,
              track_t + 3600. * 9,
              track_t + 3600. * 10
            ]
      for t in ts:
        if(t+5>tv and t-5<tv):
          gpx_map[g] = vid_file
          gpx_short.append(g)
          match_found = True
          break
      if(match_found):
        break
    if not match_found:
      print("No gpx found for: %s" % (vid_file))


  #Do not have a video:
  print(set(gpx_full).difference(set(gpx_short)))
    
  #Assert: should not print anything
  gpx_res = dict(gpx_map)
  for g in gpx_res:
    gpx_res[g] = False
  for vid_file in vid:
    if vid_file in gpx_map.values():
      for g in gpx_map:
        if gpx_map[g] == vid_file:
          gpx_res[g] = True
          break
  for g in gpx_res:
    if not gpx_res[g]:
      print(g, gpx_res[g])

  with open("gpx_match.json", 'w') as f:
    json.dump(gpx_map, f, indent = 2)
# find_videos_for_gpx()

def encode_video():
  '''
  ffmpeg -y -threads '1' -i 'in.flv' \
  -map '0:v' -map '0:a' -c:v 'libx264' -c:a 'libvo_aacenc' out.mp4 \
  -map '0:v' -map '0:a' -c:v 'libvpx' -c:a 'libvorbis' out.webm \
  -map '0:v' -r '1/4' 'out-%d.png'
  '''
  VID = r"/media/green/F0D449C8D4499230/video_sony"
  fileslist = []
  for root, dirs, files in os.walk(VID):
    for f in files:
      if f.endswith(".MP4"):
        fullname = os.path.join(VID, f)
        if os.path.exists(fullname):
          fileslist.append(fullname)

  for f in fileslist:
    # cmd = [ "ffmpeg", "-y", "-threads", "1", "-i", f,
    #         "-map", "0:v", "-map", "-vcodec", "libvpx", f.replace(".MP4", ".webm"),
    #         "-map", "-f", "mjpeg", "-vframes", "1", "-ss", "2", f.replace(".MP4", ".jpg")]
    cmd = [ "ffmpeg", "-y", "-threads", "3", "-i", f,
            "-c:a", "libvorbis", "-vcodec", "libvpx", "-vf", "scale=iw*.5:ih*.5", "-crf", "8", "-b:v", "1.5M", f.replace(".MP4", ".webm")]      
    subprocess.call(cmd)
#encode_video()

def generate_thumbnails():
  VID = r"/media/green/F0D449C8D4499230/video_sony"
  fileslist = []
  for root, dirs, files in os.walk(VID):
    for f in files:
      if f.endswith(".MP4"):
        fullname = os.path.join(VID, f)
        if os.path.exists(fullname):
          fileslist.append(fullname)
  for f in fileslist:
    print(f)
    result = subprocess.Popen(["ffprobe", "-v", "error", "-select_streams", "v:0", "-show_entries", "stream=duration", "-of", "default=nokey=1:noprint_wrappers=1", f],
      stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
    duration = float(result.stdout.readlines()[0])
    cmd = [ "ffmpeg", "-y", "-i", f, "-ss", str(duration/2.), "-vframes", "1", f.replace(".MP4", ".jpg")]      
    subprocess.call(cmd)

# generate_thumbnails()
